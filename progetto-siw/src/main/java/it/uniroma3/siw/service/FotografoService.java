package it.uniroma3.siw.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.model.Fotografo;
import it.uniroma3.siw.repository.FotografoRepository;



@Service
public class FotografoService {
	
	@Autowired
	private FotografoRepository fotografoRepository;
	
	@Transactional
	public Fotografo inserisciFotografo(Fotografo fotografo) {
		return this.fotografoRepository.save(fotografo);
	}
	
	@Transactional
	public void cancellaFotografo(Fotografo fotografo) {
		fotografoRepository.delete(fotografo);
	}

	
	@Transactional	
	public List<Fotografo> tuttiFotografi(){
		return (List<Fotografo>) this.fotografoRepository.findAll();
	}
	
	@Transactional
	public Fotografo fotografoPerID(Long id) {
		return this.fotografoRepository.findById(id).get();
	}
	@Transactional
	public boolean alreadyExists(Fotografo fotografo) {
		List<Fotografo> fotografos = this.fotografoRepository.findByEmailAndNomeAndCognome(fotografo.getEmail(), fotografo.getNome(), fotografo.getEmail());
		if (fotografos.size() > 0)
			return true;
		else 
			return false;
	}	
	
	@Transactional
	public List<Fotografo> fotografoPerEmail(String email) {
		return fotografoRepository.findByEmail(email);
	}

}
