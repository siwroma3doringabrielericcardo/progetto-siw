package it.uniroma3.siw.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.model.RichiestaFoto;
import it.uniroma3.siw.repository.RichiestaFotoRepository;



@Transactional
@Service
public class RichiestaFotoService {

	@Autowired
	private RichiestaFotoRepository richiestaRepository;

	public RichiestaFoto inserisci(RichiestaFoto arg0) {
		return richiestaRepository.save(arg0);
	}

	public void cancella(RichiestaFoto arg0) {
		richiestaRepository.delete(arg0);
	}

	public List<RichiestaFoto> trovaTutti() {
		return (List<RichiestaFoto>) richiestaRepository.findAll();
	}

	public RichiestaFoto trovaPerID(Long id) {
		Optional<RichiestaFoto> richiestaFoto = this.richiestaRepository.findById(id);
		if (richiestaFoto.isPresent()) 
			return richiestaFoto.get();
		else
			return null;
	}
	
}
