package it.uniroma3.siw.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.model.Fotografia;
import it.uniroma3.siw.repository.FotografiaRepository;


@Service
public class FotografiaService {

	@Autowired
	private FotografiaRepository fotografiaRepository;
	
	public Fotografia inserisciFotografia(Fotografia fotografia) {
		return fotografiaRepository.save(fotografia);
	}

	public void cancellaFotografia(Fotografia fotografia) {
		fotografiaRepository.delete(fotografia);
	}

	public List<Fotografia> tuttFotografie() {
		return (List<Fotografia>) fotografiaRepository.findAll();
	}

	public List<Fotografia> fotografiePerNome(String nome) {
		return fotografiaRepository.findByNome(nome);
	}

	public Fotografia fotografiaPerId(Long id) {
		Optional<Fotografia> fotografia = this.fotografiaRepository.findById(id);
		if (fotografia.isPresent()) 
			return fotografia.get();
		else
			return null;
	}
	
	
	
}
