package it.uniroma3.siw.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.model.AlbumFoto;
import it.uniroma3.siw.repository.AlbumFotoRepository;





@Service
public class AlbumFotoService {
	
	@Autowired
	private AlbumFotoRepository albumFotoRepository;
	
	@Transactional
	public AlbumFoto inserisciAlbum(AlbumFoto album) {
		return albumFotoRepository.save(album);
	}
	@Transactional
	public void cancellaAlbum(AlbumFoto album) {
		albumFotoRepository.delete(album);
	}
	
	@Transactional
	public List<AlbumFoto> tuttiAlbum() {
		return (List<AlbumFoto>) albumFotoRepository.findAll();
	}
	
	@Transactional
	public List<AlbumFoto> albumPerNome(String nome) {
		return albumFotoRepository.findByNome(nome);
	}
	
	@Transactional
	public AlbumFoto albumPerId(Long id) {
		Optional<AlbumFoto> album = this.albumFotoRepository.findById(id);
		if (album.isPresent()) 
			return album.get();
		else
			return null;
	}

}
