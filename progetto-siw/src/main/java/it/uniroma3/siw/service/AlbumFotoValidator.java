package it.uniroma3.siw.service;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.model.AlbumFoto;

@Component
public class AlbumFotoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return AlbumFoto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");

	}

}
