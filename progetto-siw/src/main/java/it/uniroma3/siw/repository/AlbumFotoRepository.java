package it.uniroma3.siw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.model.AlbumFoto;

public interface AlbumFotoRepository extends CrudRepository<AlbumFoto, Long> {
	
	public List<AlbumFoto> findByNome(String nome);
}
