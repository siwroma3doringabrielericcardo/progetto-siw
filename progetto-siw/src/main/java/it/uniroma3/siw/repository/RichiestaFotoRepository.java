package it.uniroma3.siw.repository;

import org.springframework.data.repository.CrudRepository;


import it.uniroma3.siw.model.RichiestaFoto;

public interface RichiestaFotoRepository extends CrudRepository<RichiestaFoto, Long> {

}
