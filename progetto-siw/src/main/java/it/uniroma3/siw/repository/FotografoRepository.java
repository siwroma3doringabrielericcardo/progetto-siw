package it.uniroma3.siw.repository;



import java.util.List;

import org.springframework.data.repository.CrudRepository;


import it.uniroma3.siw.model.Fotografo;

public interface FotografoRepository extends CrudRepository<Fotografo, Long> {
	
	public Fotografo findByNomeAndCognome(String nome, String cognome);
	
	public List<Fotografo> findByNome (String nome);
	
	public List<Fotografo> findByCognome (String cognome);

	public List<Fotografo> findByEmail(String email);
	
	public List<Fotografo> findByEmailAndNomeAndCognome(String email, String nome, String cognome);
}
