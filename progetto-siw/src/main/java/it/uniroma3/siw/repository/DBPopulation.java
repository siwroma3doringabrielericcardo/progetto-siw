package it.uniroma3.siw.repository;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import it.uniroma3.siw.model.AlbumFoto;
import it.uniroma3.siw.model.Fotografia;
import it.uniroma3.siw.model.Fotografo;



/*
 * è un componente della nostra applicazione
 */
@Component
public class DBPopulation implements ApplicationRunner{

	@Autowired
	private AlbumFotoRepository albumRepository;
	@Autowired
	private FotografoRepository FotografoRepository;
	@Autowired
	private FotografiaRepository fotoRepository;
	
	public void run(ApplicationArguments args) throws Exception {
		this.deleteAll();
		this.addAll();
	}
	
	private void deleteAll() {
		System.out.print("Cancello tutto");
		albumRepository.deleteAll();
		FotografoRepository.deleteAll();
		fotoRepository.deleteAll();
	}
	
	private void addAll() throws InterruptedException {
		Fotografo autore1 = null, autore2 = null;
		AlbumFoto album1 = null,album2 = null;
		Fotografia f1,f2,f3,f4,f5,f6,f7,f8,f9,f10;
		
		
		
		/*La generazione delle liste non è importante nel salvataggio iniziale */
		
		/*Inizializzo Fotografo*/
		autore1 = fotografoSet("Gabriele", null);
		autore2 = fotografoSet("Riccardo", null);
		
		/*Salvataggio autori*/
		System.out.println("Fotografo id:"+autore1.getId()+" Fotografo rif: " + autore1.toString());
		autore1 = this.FotografoRepository.save(autore1);
		System.out.println("Fotografo id:"+autore1.getId()+" Fotografo rif: " + autore1.toString());
		autore2 = this.FotografoRepository.save(autore2);
		
		/*Inizializzo Album*/
		album1 = albumSet("topolinia", null, autore1);
		album2 = albumSet("paperopoli", null,  autore2);
		
		/*Salvataggio album*/
		album1 = this.albumRepository.save(album1);
		album2 = this.albumRepository.save(album2);
		
	
		/*Inizializzo Foto*/
		f1 = fotoSet("pippo", album1, "https://i.pinimg.com/564x/40/17/88/4017885fe9d890108f3d91ca473d1250.jpg");
		f2 = fotoSet("pluto", album1, "https://i.pinimg.com/564x/b8/9a/9e/b89a9e6abfee7cccf66fab3e0203b41a.jpg");
		f3 = fotoSet("paperino", album1, "https://i.pinimg.com/564x/0f/fe/3a/0ffe3a0080b803d865717245557dcf95.jpg");
		f4 = fotoSet("ziopaperone", album1, "https://i.pinimg.com/564x/d9/34/c8/d934c89110e134c9e943e4575ecb9576.jpg");
		f5 = fotoSet("topolina", album1, "https://i.pinimg.com/564x/fd/f8/47/fdf847fdacfaebf8e9441d621c29feab.jpg");
		f6 = fotoSet("topolino", album2, "https://i.pinimg.com/564x/7d/62/75/7d62759b503d9005892a592696055faf.jpg");
		f7 = fotoSet("paperoga", album2, "https://i.pinimg.com/236x/27/66/6a/27666a06f4fcde2f6b40cc0e0c69357a.jpg");
		f8 = fotoSet("gastone", album2, "https://i.pinimg.com/236x/9a/66/f8/9a66f8f16b93c80773be430a03bb91cf.jpg");
		f9 = fotoSet("nonnnapapera", album2, "https://i.pinimg.com/564x/ee/27/02/ee2702c07c065e20022a5d6a80b29fba.jpg");
		f10 = fotoSet("paperina", album2, "https://i.pinimg.com/564x/bc/00/f5/bc00f5e4f13d36ea3f602de63ddb64c8.jpg");

		/*Salvataggio foto*/
		f1 = this.fotoRepository.save(f1);
		f2 = this.fotoRepository.save(f2);
		f3 = this.fotoRepository.save(f3);
		f4 = this.fotoRepository.save(f4);
		f5 = this.fotoRepository.save(f5);
		f6 = this.fotoRepository.save(f6);
		f7 = this.fotoRepository.save(f7);
		f8 = this.fotoRepository.save(f8);
		f9 = this.fotoRepository.save(f9);
		f10 = this.fotoRepository.save(f10);

	}
	
	

	private Fotografia fotoSet(String nomeFoto, AlbumFoto albFoto, String linkFoto) {
		Fotografia f = new Fotografia();
		f.setNome(nomeFoto);
		f.setAlbumFoto(albFoto);
		f.setLink(linkFoto);
		return f;
	}
	
	private AlbumFoto albumSet(String nomeAlbum, List<Fotografia> fotoAlbum, Fotografo FotografoAlbum) {
		AlbumFoto a = new AlbumFoto();
		a.setNome(nomeAlbum);
		a.setFotografie(fotoAlbum);
		a.setFotografo(FotografoAlbum);
		return a;
	}
	private Fotografo fotografoSet(String nomeFotografo, List<AlbumFoto> albumFotografo) {
		Fotografo a = new Fotografo();
		a.setNome(nomeFotografo);
		a.setAlbumFoto(albumFotografo);
		return a;
	}
	

}
