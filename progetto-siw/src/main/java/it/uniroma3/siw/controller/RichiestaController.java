package it.uniroma3.siw.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.model.RichiestaFoto;
import it.uniroma3.siw.service.RichiestaFotoService;
import it.uniroma3.siw.service.RichiestaFotoValidator;


@Controller
public class RichiestaController {
	
    @Autowired
    private RichiestaFotoService richiestaService;
    @Autowired
    private RichiestaFotoValidator validator;
    
    @RequestMapping(value = "/richiesta/{id}", method = RequestMethod.GET)
    public String getRichiesta(@PathVariable("id") Long id, Model model) {
        model.addAttribute("richiesta", this.richiestaService.trovaPerID(id));
    	return "visualizzaordine";
    }
    
    @RequestMapping("/richieste")
    public String richieste(Model model) {
        model.addAttribute("richieste", this.richiestaService.trovaTutti());
        return "richieste";
    }

    @RequestMapping("/inseriscirichiesta")
    public String aggiungiRichiesta(Model model) {
        model.addAttribute("richiesta", new RichiestaFoto());
        return "formrichiesta";
    }
    
    @RequestMapping(value = "/richiesta", method = RequestMethod.POST)
    public String newRichiesta(@Valid @ModelAttribute("richiesta") RichiestaFoto richiesta, 
    									Model model, BindingResult bindingResult) {
        this.validator.validate(richiesta, bindingResult);
        
        if (!bindingResult.hasErrors()) {
        	this.richiestaService.inserisci(richiesta);
        	model.addAttribute("richieste", this.richiestaService.trovaTutti());
            return "richiestainserita";
        }
        return "formrichiesta";
    }

}
