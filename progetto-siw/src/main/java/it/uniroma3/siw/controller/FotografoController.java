package it.uniroma3.siw.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import it.uniroma3.siw.model.Fotografo;
import it.uniroma3.siw.service.FotografoService;
import it.uniroma3.siw.service.FotografoValidator;

@Controller
public class FotografoController {

	@Autowired
	private FotografoService fotografoService;
	@Autowired
	private FotografoValidator fotografoValidator;

	@RequestMapping(value ="/inseriscifotografo", method = RequestMethod.GET)
	public String getForm(Model model) {
		model.addAttribute("fotografo", new Fotografo());
		return "formfotografo";
	}

	@RequestMapping(value = "/fotografo/{id}", method = RequestMethod.GET)
	public String getFotografo(@PathVariable("id") Long id, Model model) {
		model.addAttribute("fotografo", this.fotografoService.fotografoPerID(id));
		return "fotografo";
	}

	@RequestMapping("/fotografi")
	public String fotografi(Model model) {
		model.addAttribute("fotografi", this.fotografoService.tuttiFotografi());
		return "fotografi.html";
	}

	@RequestMapping(value = "/inseriscifotografo", method = RequestMethod.POST)
    public String newFotografo(@Valid @ModelAttribute("fotografo") Fotografo fotografo, 
    									Model model, BindingResult bindingResult) {
        this.fotografoValidator.validate(fotografo , bindingResult);
        
            if (!bindingResult.hasErrors()) {
                this.fotografoService.inserisciFotografo(fotografo);
                model.addAttribute("fotografi", this.fotografoService.tuttiFotografi());
                return "fotografi";
            }
        return"formfotografo";
	}
        
  
}
