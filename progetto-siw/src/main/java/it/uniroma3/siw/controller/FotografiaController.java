package it.uniroma3.siw.controller;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.model.Fotografia;
import it.uniroma3.siw.service.FotografiaService;
import it.uniroma3.siw.service.FotografiaValidator;



@Controller
public class FotografiaController {
	
	@Autowired
    private FotografiaService fotografiaService;
    @Autowired
    private FotografiaValidator validator;

	
	public static String uploadDirectory = System.getProperty("user.dir") + "/up";
	
	@RequestMapping("/tornaindex")
	public String backHome() {
			return "index";
	}
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	
	 @RequestMapping("/fotografie")
	    public String fotografie(Model model) {
	        model.addAttribute("fotografie", this.fotografiaService.tuttFotografie());
	        return "tuttelefoto";
	    }
	 
	 
	 @RequestMapping(value = "/fotografia/{id}", method = RequestMethod.GET)
	    public String getFotografia(@PathVariable("id") Long id, Model model) {
	        model.addAttribute("fotografia", this.fotografiaService.fotografiaPerId(id));
	    	return "fotografia";
	    }
	    
	 @RequestMapping(value = "/fotografia", method = RequestMethod.POST)
	    public String newFotografia(@Valid @ModelAttribute("fotografia") Fotografia fotografia, 
	    									Model model, BindingResult bindingResult) {
	        this.validator.validate(fotografia, bindingResult);
	            if (!bindingResult.hasErrors()) {
	                this.fotografiaService.inserisciFotografia(fotografia);
	                model.addAttribute("fotografie", this.fotografiaService.tuttFotografie());
	                return "tuttelefoto";
	            }
	        
	        return "fotografiaForm";
	    }
	 
	 @RequestMapping("/inseriscifotografia")
 	public String getFormFotografo(Model model) {
     	model.addAttribute("fotografia", new Fotografia());
 			return "formfotografia";
	 }
}


