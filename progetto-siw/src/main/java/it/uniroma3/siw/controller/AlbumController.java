package it.uniroma3.siw.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.model.AlbumFoto;
import it.uniroma3.siw.service.AlbumFotoService;
import it.uniroma3.siw.service.AlbumFotoValidator;


@Controller
public class AlbumController {

	@Autowired
	private AlbumFotoService albumFotoService;
	@Autowired
	private AlbumFotoValidator albumFotoValidator;
	

	@RequestMapping(value = "/album/{id}", method = RequestMethod.GET)
	public String getAlbum(@PathVariable("id") Long id, Model model) {
		model.addAttribute("album", this.albumFotoService.albumPerId(id));
		return "album";
	}

	@RequestMapping("/tuttiglialbum")
	public String albums(Model model) {
		model.addAttribute("albums", this.albumFotoService.tuttiAlbum());
		return "albums";
	}

	@RequestMapping(value="/inseriscialbum", method = RequestMethod.GET)
	public String aggiungiAlbum(Model model) {
		model.addAttribute("album", new AlbumFoto());
		return "formalbum";
	}

	@RequestMapping(value = "/album", method = RequestMethod.POST)
	public String newAlbum(@Valid @ModelAttribute("album") AlbumFoto album, Model model, BindingResult bindingResult) {
		this.albumFotoValidator.validate(album, bindingResult);

		if (!bindingResult.hasErrors()) {
			this.albumFotoService.inserisciAlbum(album);
			model.addAttribute("albums", this.albumFotoService.tuttiAlbum());
			return "albumList";
		}

		return "albumForm";
	}
}
