package it.uniroma3.siw.controller;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class FunzionarioController {

	

	@RequestMapping(value="/login")
	public String linkLogin(){
		return "login";
	}

	@RequestMapping(value="/logout")
	public String linkLogout() {
	return "index";
	}
	
	
	@RequestMapping("/indexfunzionario")
	public String indexFunzionario() {
			return "indexfunzionario";
	}
	
	
	
}

