package it.uniroma3.siw.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Funzionario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@OneToMany
	private List<RichiestaFoto> richiestaFoto;
	
	@OneToMany
	private List<Fotografo> fotografo;
	
	
	private String email;
	private String password;
	private String nome;
	private String cognome;
	
	
	
	
	
	
	public Funzionario(String email, String password, String nome, String cognome) {
		super();
		this.email = email;
		this.password = password;
		this.nome = nome;
		this.cognome = cognome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RichiestaFoto> getRichiestaFoto() {
		return richiestaFoto;
	}
	public void setRichiestaFoto(List<RichiestaFoto> richiestaFoto) {
		this.richiestaFoto = richiestaFoto;
	}
	public List<Fotografo> getFotografo() {
		return fotografo;
	}
	public void setFotografo(List<Fotografo> fotografo) {
		this.fotografo = fotografo;
	}
	public String getUsername() {
		return email;
	}
	public void setUsername(String username) {
		this.email = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	
	
	

}
