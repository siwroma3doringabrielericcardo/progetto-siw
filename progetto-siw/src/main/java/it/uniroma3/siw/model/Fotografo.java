package it.uniroma3.siw.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Fotografo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	
	
	
	private String nome;
	
	private String cognome;
	
	private String email;

	
	@OneToMany (mappedBy="fotografo")
	private List<Fotografia> fotografia;
	
	@OneToMany (mappedBy="fotografo")
	private List<AlbumFoto> albumFoto;
	
	public Fotografo() {
		
	}

	public Fotografo( String nome, String cognome , String email) {
		
		this.nome = nome;
		this.cognome = cognome;
		this.email=email;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Fotografia> getFotografia() {
		return fotografia;
	}

	public void setFotografia(List<Fotografia> fotografia) {
		this.fotografia = fotografia;
	}

	public List<AlbumFoto> getAlbumFoto() {
		return albumFoto;
	}

	public void setAlbumFoto(List<AlbumFoto> albumFoto) {
		this.albumFoto = albumFoto;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	
	
	
	
}