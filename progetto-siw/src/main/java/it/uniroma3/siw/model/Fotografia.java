package it.uniroma3.siw.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Fotografia {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Long id;
	
	 @ManyToOne
	 private Fotografo fotografo;
	 
	 @ManyToOne
	 private AlbumFoto albumFoto;
	 
	
	 private String nome;
	 
	 private String link;

	 
	 
	 
	public Fotografia(String nome, AlbumFoto albumFoto, String link, Fotografo fotografo) {
		super();
		this.fotografo = fotografo;
		this.albumFoto = albumFoto;
		this.nome = nome;
		this.link = link;
	}
	public Fotografia(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Fotografo getFotografo() {
		return fotografo;
	}

	public void setFotografo(Fotografo fotografo) {
		this.fotografo = fotografo;
	}

	public AlbumFoto getAlbumFoto() {
		return albumFoto;
	}

	public void setAlbumFoto(AlbumFoto albumFoto) {
		this.albumFoto = albumFoto;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	
	 
}
