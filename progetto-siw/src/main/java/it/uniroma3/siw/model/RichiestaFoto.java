package it.uniroma3.siw.model;

//import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.OneToMany;

@Entity
public class RichiestaFoto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	//@OneToMany
	//private List<Fotografia> fotografie;
	
	private String richiedente;
	private String emailContatto;
	private String scopo;
	private String listaFoto;
	
	
	
	public RichiestaFoto() {
	}
	
	
	public RichiestaFoto(String richiedente, String emailContatto, String scopo) {
		super();
		this.richiedente = richiedente;
		this.emailContatto = emailContatto;
		this.scopo = scopo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	/*public List<Fotografia> getFotografie() {
		return fotografie;
	}
	public void setFotografie(List<Fotografia> fotografie) {
		this.fotografie = fotografie;
	}
	*/
	
	public String getRichiedente() {
		return richiedente;
	}


	public void setRichiedente(String richiedente) {
		this.richiedente = richiedente;
	}


	public String getEmailContatto() {
		return emailContatto;
	}
	public void setEmailContatto(String emailContatto) {
		this.emailContatto = emailContatto;
	}
	public String getScopo() {
		return scopo;
	}
	public void setScopo(String scopo) {
		this.scopo = scopo;
	}


	public String getListaFoto() {
		return listaFoto;
	}


	public void setListaFoto(String listaFoto) {
		this.listaFoto = listaFoto;
	}
	
	
	
}
